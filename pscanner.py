#!/usr/bin/python3
import socket 

ip = input("Enter the ip: ")
start_port = int(input("Enter Start port: "))
end_port = int(input("Enter End port: "))


while (start_port <= end_port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    
    if s.connect_ex((ip, start_port)): 
        start_port += 1
        continue
        # print ("Port", start_port, "is closed" ) 
    else: 
        print ("Port", start_port, "is open")
    
    start_port += 1
